# pre-commit-hook

Oslandia pre-commit hooks for [pre-commit](https://pre-commit.com).

Available hook:

* [pgFormatter](https://github.com/darold/pgFormatter)

## Installation

### pgFormatter

Just add this yaml into your `.pre-commit-config.yaml`:

```yaml
  - repo: https://gitlab.com/Oslandia/pre-commit-hook
    rev: 0.0.1
    hooks:
      - id: pgFormatter
        args:
          [
            "-s4 -g -t -w120 -W5"
          ]
```

You will also need to have the `pg_format` binary available.

## License

GNU GPLv3
