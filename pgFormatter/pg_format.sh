#!/usr/bin/env bash
set -e

options=""
while [[ "$1" =~ -[a-z].* ]]
do
    options="$options $1"
    shift
done

# echo "options: $options"
# echo "remains: $@"

for filename in "$@"
do
    t=$(mktemp)
    pg_format $options "$filename" > "$t"
    diff -q "$t" "$filename" > /dev/null || echo "Fixing $filename"
    mv "$t" "$filename"
done
